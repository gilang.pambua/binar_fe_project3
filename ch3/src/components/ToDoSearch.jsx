import React,{useState} from "react";
import data from "./data.json";
import {useNavigate} from "react-router-dom"

function ToDoSearch (){ 
    const [searchQuery, setSearchQuery] = useState("");
    const [searchResult, setSearchResult] = useState([]);
    const [showResults, setShowResults] = useState(false);

    const handleSearch = (event) => {
        const query = event.target.value;
        setSearchQuery(query);
        setShowResults(false);
    
        const result = data.filter((item) => {
          return item.task.toLowerCase().includes(query.toLowerCase());
        });
    
        setSearchResult(result);
    };
    const handleSearchClick = () => {
        const results = data.filter((item) =>
          item.task.toLowerCase().includes(searchQuery.toLowerCase())
        );
        setSearchResult(results);
        setShowResults(true);
    };
    const navigate = useNavigate();

  return (
    <div className="flex justify-center py-[20px]">
      <div className="flex flex-col items-center w-full max-w-[768px]">
        <h1 className="text-[24px] font-[600]">TodoSearch</h1>
        <div className="flex justify-between my-[10px] p-[20px] border-[1px] border-gray rounded-[5px] w-full">
            <div className="w-[55%]">
                <div className="flex w-full ">
                    <button className="grid place-items-center p-[5px] rounded-l-[3px] bg-cyan w-[35px] h-[35px] ">
                        <svg xmlns="http://www.w3.org/2000/svg" className="w-[20px] h-[auto]" viewBox="0 0 20 20" id="search"><g fill="none" fill-rule="evenodd" stroke="#fff" stroke-linecap="round" stroke-linejoin="round" stroke-width="2" transform="translate(1 1)"><circle cx="7.5" cy="7.5" r="7.5"/><path d="M18 18l-5.2-5.2"/></g></svg>
                    </button>
                    <input value={searchQuery} onChange={handleSearch} type="text" placeholder="Search To Do" className="w-full p-[5px] px-[10px] border-[1px] border-gray rounded-r-[3px] text-[13px]"/>
                </div>
                {showResults && (
                    <div className="mt-[20px] p-[15px] px-[20px] border-[1px] border-gray rounded-[5px] w-full flex justify-between item-center mb-[15px]">
                        {searchResult.length > 0 ? (
                            <ul>
                                {searchResult.map((item) => (
                                    <li key={item.id}>{item.task}</li>
                                ))}
                            </ul>
                        ) : (
                            <p>No Tasks</p>
                        )}  
                    </div>
                )}
   
                <div className="w-full mt-[10px]">
                    <button onClick={handleSearchClick} className="grid place-items-center p-[7px] rounded-[3px] bg-cyan w-full text-[white] text-[14px]">
                        Search
                    </button>
                </div>
            </div>
            <div className="w-[30%] flex flex-col justify-end">
                <div className="todoAdd w-full">
                    <button onClick={()=>navigate("/add")} className="grid place-items-center p-[7px] rounded-[3px] bg-cyan w-full text-[white] text-[14px]">
                        Add new Task
                    </button>
                </div>
            </div>
        </div>
      </div>
    </div>
  );
};

export default ToDoSearch;
