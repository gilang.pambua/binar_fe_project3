import React, { useState } from "react";
import data from "./data.json";

function ToDoList(){
    const [tasks, setTasks] = useState(data);
   
    const handleShowAllTasks = () => {
        setTasks(data);
    };
    
    const handleShowDoneTasks = () => {
        setTasks(data.filter((task) => task.complete));
    };
    
    const handleShowUndoneTasks = () => {
        setTasks(data.filter((task) => !task.complete));
    };

    const handleToggle = (id) => {
        const updatedTasks = tasks.map((task) => task.id === id ? { ...task, complete: !task.complete } : task);
        setTasks(updatedTasks);
    };

    const handleDelete = (id) => {
        const updatedTasks = tasks.filter((task) => task.id !== id);
        setTasks(updatedTasks);
    };
    
    const handleEdit = (id, newTask) => {
      const updatedTasks = tasks.map((task) =>
        task.id === id ? { ...task, task: newTask } : task);
        setTasks(updatedTasks);
    };

    const handleDeleteAllDone = () => {
        setTasks(tasks.filter((task) => !task.complete));
    };  
    const handleDeleteAllTasks = () => {
        setTasks([]);
    };

    return (
        <div className="flex justify-center">
            <div className="flex flex-col items-center w-full max-w-[768px]">
                <h1 className="text-[24px] font-[600]">TodoList</h1>
                <div className="w-full mt-[10px] flex justify-between gap-[30px] mb-[25px]">
                    <button onClick={handleShowAllTasks} className='grid place-items-center p-[7px] rounded-[3px] bg-cyan w-full text-[white] text-[14px] '>
                        All
                    </button>
                    <button onClick={handleShowDoneTasks} className='grid place-items-center p-[7px] rounded-[3px] bg-cyan w-full text-[white] text-[14px] '>
                        Done
                    </button>
                    <button onClick={handleShowUndoneTasks} className='grid place-items-center p-[7px] rounded-[3px] bg-cyan w-full text-[white] text-[14px] '>
                        Todo
                    </button>
                </div>
                <div className='mt-[15px] w-full'>
                    {tasks.map((task) => (
                    <div key={task.id} className="p-[15px] px-[20px] border-[1px] border-gray rounded-[5px] w-full flex justify-between item-center mb-[15px]">
                        <p className='text-[14px]'>{task.task}</p>
                        <div className="flex gap-[10px] items-center">
                            <input type='checkbox' id={`task-${task.id}`} checked={task.complete} onChange={() => handleToggle(task.id)}/>
                            <button onClick={() => handleEdit(task.id, prompt("Enter New Task"))}>
                                <svg xmlns="http://www.w3.org/2000/svg" className='w-[20px] h-[auto]' viewBox="0 0 18 18" version="1.1">
                                    <g id="out" stroke="none" stroke-width="1" fill="none">
                                        <path d="M2.25,12.9378906 L2.25,15.75 L5.06210943,15.75 L13.3559575,7.45615192 L10.5438481,4.64404249 L2.25,12.9378906 L2.25,12.9378906 L2.25,12.9378906 Z M15.5306555,5.28145396 C15.8231148,4.98899458 15.8231148,4.5165602 15.5306555,4.22410082 L13.7758992,2.46934454 C13.4834398,2.17688515 13.0110054,2.17688515 12.718546,2.46934454 L11.3462366,3.84165394 L14.1583461,6.65376337 L15.5306555,5.28145396 L15.5306555,5.28145396 L15.5306555,5.28145396 Z" id="path" fill="#F0C24F" ></path>
                                    </g>
                                </svg>
                            </button>
                            <button onClick={() => handleDelete(task.id)}>
                                <svg xmlns="http://www.w3.org/2000/svg" className='w-[15px] h-[auto]' version="1.1" id="_x32_" viewBox="0 0 512 512" fill="#D73549" stroke="#D73549" >
                                    <path class="st0" d="M88.594,464.731C90.958,491.486,113.368,512,140.234,512h231.523c26.858,0,49.276-20.514,51.641-47.269 l25.642-335.928H62.952L88.594,464.731z M329.183,221.836c0.357-5.876,5.4-10.349,11.277-9.992 c5.877,0.357,10.342,5.409,9.993,11.277l-10.129,202.234c-0.357,5.876-5.4,10.342-11.278,9.984 c-5.876-0.349-10.349-5.4-9.992-11.269L329.183,221.836z M245.344,222.474c0-5.885,4.772-10.648,10.657-10.648 c5.885,0,10.656,4.763,10.656,10.648v202.242c0,5.885-4.771,10.648-10.656,10.648c-5.885,0-10.657-4.763-10.657-10.648V222.474z M171.531,211.844c5.876-0.357,10.92,4.116,11.278,9.992l10.137,202.234c0.357,5.869-4.116,10.92-9.992,11.269 c-5.869,0.357-10.921-4.108-11.278-9.984l-10.137-202.234C161.182,217.253,165.654,212.201,171.531,211.844z"/> <path class="st0" d="M439.115,64.517c0,0-34.078-5.664-43.34-8.479c-8.301-2.526-80.795-13.566-80.795-13.566l-2.722-19.297 C310.388,9.857,299.484,0,286.642,0h-30.651H225.34c-12.825,0-23.728,9.857-25.616,23.175l-2.721,19.297 c0,0-72.469,11.039-80.778,13.566c-9.261,2.815-43.357,8.479-43.357,8.479C62.544,67.365,55.332,77.172,55.332,88.38v21.926h200.66 h200.676V88.38C456.668,77.172,449.456,67.365,439.115,64.517z M276.318,38.824h-40.636c-3.606,0-6.532-2.925-6.532-6.532 s2.926-6.532,6.532-6.532h40.636c3.606,0,6.532,2.925,6.532,6.532S279.924,38.824,276.318,38.824z"/>
                                </svg>
                            </button>
                        </div>
                    </div>
                    ))}
                </div>
                <div className="w-full mt-[10px] flex justify-between gap-[30px] mb-[25px]">
                    <button onClick={handleDeleteAllDone} className='grid place-items-center p-[7px] rounded-[3px] bg-red w-full text-[white] text-[14px]'>
                        Delete Done Text 
                    </button>
                    <button onClick={handleDeleteAllTasks} className='grid place-items-center p-[7px] rounded-[3px] bg-red w-full text-[white] text-[14px]'>
                        Delete All Text
                    </button>
                </div>
            </div>
        </div>
        
    )
}

export default ToDoList;