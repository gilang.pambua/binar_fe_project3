import React from 'react'

function ToDoInput(){
  return (
    <div className="flex justify-center py-[20px]">
      <div className="flex flex-col items-center w-full max-w-[768px]">
        <h1 className="text-[24px] font-[600]">TodoInput</h1>
        <div className="flex justify-between my-[10px] p-[20px] border-[1px] border-gray rounded-[5px] w-full">
            <div className="w-[100%]">
                <div className="flex w-full ">
                    <button className="grid place-items-center p-[5px] rounded-l-[3px] bg-cyan w-[35px] h-[35px] ">
                    <svg xmlns="http://www.w3.org/2000/svg" className='w-[20px] h-[auto]' viewBox="0 0 24 24" fill="none">
                        <path d="M3.5 18V7C3.5 3 4.5 2 8.5 2H15.5C19.5 2 20.5 3 20.5 7V17C20.5 17.14 20.5 17.28 20.49 17.42" stroke="#ffffff" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"/> <path d="M6.35 15H20.5V18.5C20.5 20.43 18.93 22 17 22H7C5.07 22 3.5 20.43 3.5 18.5V17.85C3.5 16.28 4.78 15 6.35 15Z" stroke="#ffffff" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"/> <path d="M8 7H16" stroke="#ffffff" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"/> <path d="M8 10.5H13" stroke="#ffffff" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"/>
                    </svg>
                    </button>
                    <input type="text" placeholder="Input To Do" className="w-full p-[5px] px-[10px] border-[1px] border-gray rounded-r-[3px] text-[13px]"/>
                </div>
                <div className="w-full mt-[10px]">
                    <button className="grid place-items-center p-[7px] rounded-[3px] bg-cyan w-full text-[white] text-[14px]">
                        Submit
                     </button>
                </div>
            </div>
        </div>
      </div>
    </div>
  )
}

export default ToDoInput