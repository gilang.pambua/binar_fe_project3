import React from 'react'
import ToDoList from './components/ToDoList'
import ToDoSearch from './components/ToDoSearch'
import ToDoInput from './pages/ToDoInput'
import { BrowserRouter as Router,Routes, Route } from 'react-router-dom'

const App = () => {
  return (
    <div className='font-poppins'>
      <Router>
        <Routes>
          <Route path="" element={<ToDoSearch />}></Route>
          <Route path="/add" element={<ToDoInput />}></Route>
        </Routes>
      </Router>
      <ToDoList />
    </div>
  )
}

export default App